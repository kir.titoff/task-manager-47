package ru.t1.ktitov.tm.api.service.dto;

import ru.t1.ktitov.tm.api.repository.dto.IDtoRepository;
import ru.t1.ktitov.tm.dto.model.AbstractModelDTO;

public interface IDtoService<M extends AbstractModelDTO> extends IDtoRepository<M> {
}
