package ru.t1.ktitov.tm.api.service.model;

import ru.t1.ktitov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
