package ru.t1.ktitov.tm.api.service.model;

import ru.t1.ktitov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M>, IUserOwnedRepository<M> {
}
