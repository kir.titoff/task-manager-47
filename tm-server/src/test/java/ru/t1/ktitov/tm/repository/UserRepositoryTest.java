/*
package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.repository.dto.UserRepositoryDTO;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected IUserRepositoryDTO getRepository(@NotNull EntityManager entityManager) {
        return new UserRepositoryDTO(entityManager);
    }

    private void compareUsers(@NotNull final UserDTO user1, @NotNull final UserDTO user2) {
        Assert.assertEquals(user1.getId(), user2.getId());
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getEmail(), user2.getEmail());
        Assert.assertEquals(user1.getFirstName(), user2.getFirstName());
        Assert.assertEquals(user1.getLastName(), user2.getLastName());
        Assert.assertEquals(user1.getMiddleName(), user2.getMiddleName());
        Assert.assertEquals(user1.getRole(), user2.getRole());
        Assert.assertEquals(user1.getLocked(), user2.getLocked());
    }

    private void compareUsers(
            @NotNull final List<UserDTO> userList1,
            @NotNull final List<UserDTO> userList2) {
        Assert.assertEquals(userList1.size(), userList2.size());
        for (int i = 0; i < userList1.size(); i++) {
            compareUsers(userList1.get(i), userList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(USER1.getId());
            repository.removeById(USER2.getId());
            repository.removeById(ADMIN3.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            compareUsers(USER1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN3);
            Assert.assertEquals(3, repository.getSize());
            compareUsers(USER_LIST1, repository.findAll());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN3);
            Assert.assertEquals(3, repository.getSize());
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN3);
            compareUsers(USER_LIST1, repository.findAll());
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN3);
            compareUsers(USER1, repository.findOneById(USER1.getId()));
            compareUsers(USER2, repository.findOneById(USER2.getId()));
            compareUsers(ADMIN3, repository.findOneById(ADMIN3.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN3);
            Assert.assertEquals(3, repository.getSize());
            repository.remove(USER1);
            Assert.assertEquals(2, repository.getSize());
            repository.remove(USER4);
            Assert.assertEquals(2, repository.getSize());
            repository.removeById(USER2.getId());
            Assert.assertEquals(1, repository.getSize());
            compareUsers(ADMIN3, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN3);
            compareUsers(USER1, repository.findByLogin("user1"));
            compareUsers(USER2, repository.findByLogin("user2"));
            compareUsers(ADMIN3, repository.findByLogin("user3"));
            Assert.assertNull(repository.findByLogin("admin1"));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN3);
            compareUsers(USER1, repository.findByEmail("user1@gmail.com"));
            compareUsers(USER2, repository.findByEmail("user2@gmail.com"));
            compareUsers(ADMIN3, repository.findByEmail("user3@gmail.com"));
            Assert.assertNull(repository.findByEmail("admin1@gmail.com"));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
*/
