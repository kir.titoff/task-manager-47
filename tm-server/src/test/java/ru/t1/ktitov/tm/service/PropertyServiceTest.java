/*
package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService service = new PropertyService();

    @Test
    public void getServerHost() {
        Assert.assertNotNull(service.getServerHost());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(service.getServerPort());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(service.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(service.getSessionTimeout());
    }

    @Test
    public void getPasswordIteration() {
        Assert.assertNotNull(service.getPasswordIteration());
    }

    @Test
    public void getPasswordSecret() {
        Assert.assertNotNull(service.getPasswordSecret());
    }

}
*/
